(function(scope, undefined) {

    scope.holidays = [ {
        "title": "Muscle Stat Day",
        "href" : "http://kol.coldfront.net/thekolwiki/index.php/Stat_Days",
        "dates": [
            { "month":  1, "date":  0, "type": "kol" },
            { "month":  1, "date":  1, "type": "kol" },
            { "month":  3, "date":  0, "type": "kol" },
            { "month":  3, "date":  1, "type": "kol" },
            { "month":  5, "date":  0, "type": "kol" },
            { "month":  5, "date":  1, "type": "kol" },
            { "month":  7, "date":  0, "type": "kol" },
            { "month":  7, "date":  1, "type": "kol" },
            { "month":  9, "date":  0, "type": "kol" },
            { "month":  9, "date":  1, "type": "kol" },
            { "month": 11, "date":  0, "type": "kol" },
            { "month": 11, "date":  1, "type": "kol" },
        ]
    }, {
        "title": "Mysticality Stat Day",
        "href" : "http://kol.coldfront.net/thekolwiki/index.php/Stat_Days",
        "dates": [
            { "month":  0, "date":  4, "type": "kol" },
            { "month":  1, "date":  4, "type": "kol" },
            { "month":  2, "date":  4, "type": "kol" },
            { "month":  3, "date":  4, "type": "kol" },
            { "month":  4, "date":  4, "type": "kol" },
            { "month":  5, "date":  4, "type": "kol" },
            { "month":  6, "date":  4, "type": "kol" },
            { "month":  7, "date":  4, "type": "kol" },
            { "month":  8, "date":  4, "type": "kol" },
            { "month":  9, "date":  4, "type": "kol" },
            { "month": 10, "date":  4, "type": "kol" },
            { "month": 11, "date":  4, "type": "kol" },
        ]
    }, {
        "title": "Moxie Stat Day",
        "href" : "http://kol.coldfront.net/thekolwiki/index.php/Stat_Days",
        "dates": [
            { "month":  0, "date":  0, "type": "kol" },
            { "month":  1, "date":  7, "type": "kol" },
            { "month":  2, "date":  0, "type": "kol" },
            { "month":  3, "date":  7, "type": "kol" },
            { "month":  4, "date":  0, "type": "kol" },
            { "month":  5, "date":  7, "type": "kol" },
            { "month":  6, "date":  0, "type": "kol" },
            { "month":  7, "date":  7, "type": "kol" },
            { "month":  8, "date":  0, "type": "kol" },
            { "month":  9, "date":  7, "type": "kol" },
            { "month": 10, "date":  0, "type": "kol" },
            { "month": 11, "date":  7, "type": "kol" },
        ]
    }, {
        "title": "Festival of Jarlsberg",
        "href" : "http://kol.coldfront.net/thekolwiki/index.php/Festival_of_Jarlsberg",
        "dates": [
            { "month":  0, "date":  0, "type": "kol" },
            { "month":  0, "date":  0, "type": "gregorian" }
        ]
    }, {
        "title": "Valentine's Day",
        "href" : "http://kol.coldfront.net/thekolwiki/index.php/Valentine%27s_Day",
        "dates": [
            { "month":  1, "date":  3, "type": "kol" },
            { "month":  1, "date": 13, "type": "gregorian" }
        ]
    }, {
        "title": "Groundhog Day",
        "href" : "http://kol.coldfront.net/thekolwiki/index.php/Groundhog_Day",
        "dates": [
            { "month":  1, "date": 1, "type": "gregorian" }
        ]
    }, {
        "title": "St. Sneaky Pete's Day",
        "href" : "http://kol.coldfront.net/thekolwiki/index.php/St._Sneaky_Pete%27s_Day",
        "dates": [
            { "month":  2, "date":  2, "type": "kol" },
            { "month":  2, "date": 16, "type": "gregorian" }
        ]
    }, {
        "title": "Oyster Egg Day",
        "href" : "http://kol.coldfront.net/thekolwiki/index.php/Oyster_Egg_Day",
        "dates": [
            { "month":  3, "date":  1, "type": "kol" },
            { "month":  3, "date": 22, "type": "gregorian", "year": 2000 },
            { "month":  3, "date": 14, "type": "gregorian", "year": 2001 },
            { "month":  2, "date": 30, "type": "gregorian", "year": 2002 },
            { "month":  3, "date": 19, "type": "gregorian", "year": 2003 },
            { "month":  3, "date": 10, "type": "gregorian", "year": 2004 },
            { "month":  2, "date": 26, "type": "gregorian", "year": 2005 },
            { "month":  3, "date": 15, "type": "gregorian", "year": 2006 },
            { "month":  3, "date":  7, "type": "gregorian", "year": 2007 },
            { "month":  2, "date": 22, "type": "gregorian", "year": 2008 },
            { "month":  3, "date": 11, "type": "gregorian", "year": 2009 },
            { "month":  3, "date":  3, "type": "gregorian", "year": 2010 },
            { "month":  3, "date": 23, "type": "gregorian", "year": 2011 },
            { "month":  3, "date":  7, "type": "gregorian", "year": 2012 },
            { "month":  2, "date": 30, "type": "gregorian", "year": 2013 },
            { "month":  3, "date": 19, "type": "gregorian", "year": 2014 },
            { "month":  3, "date":  4, "type": "gregorian", "year": 2015 },
            { "month":  2, "date": 26, "type": "gregorian", "year": 2016 },
            { "month":  3, "date": 15, "type": "gregorian", "year": 2017 },
            { "month":  3, "date":  0, "type": "gregorian", "year": 2018 },
            { "month":  3, "date": 20, "type": "gregorian", "year": 2019 },
            { "month":  3, "date": 11, "type": "gregorian", "year": 2020 },
            { "month":  3, "date":  3, "type": "gregorian", "year": 2021 },
            { "month":  3, "date": 16, "type": "gregorian", "year": 2022 },
            { "month":  3, "date":  8, "type": "gregorian", "year": 2023 },
            { "month":  2, "date": 30, "type": "gregorian", "year": 2024 },
            { "month":  3, "date": 19, "type": "gregorian", "year": 2025 },
            { "month":  3, "date":  4, "type": "gregorian", "year": 2026 },
            { "month":  2, "date": 27, "type": "gregorian", "year": 2027 },
            { "month":  3, "date": 15, "type": "gregorian", "year": 2028 },
            { "month":  3, "date":  0, "type": "gregorian", "year": 2029 },
            { "month":  3, "date": 20, "type": "gregorian", "year": 2030 },
            { "month":  3, "date": 12, "type": "gregorian", "year": 2031 },
            { "month":  2, "date": 27, "type": "gregorian", "year": 2032 },
            { "month":  3, "date": 16, "type": "gregorian", "year": 2033 },
            { "month":  3, "date":  8, "type": "gregorian", "year": 2034 },
            { "month":  2, "date": 24, "type": "gregorian", "year": 2035 },
            { "month":  3, "date": 12, "type": "gregorian", "year": 2036 },
            { "month":  3, "date":  4, "type": "gregorian", "year": 2037 },
            { "month":  3, "date": 24, "type": "gregorian", "year": 2038 },
            { "month":  3, "date":  9, "type": "gregorian", "year": 2039 },
            { "month":  3, "date":  0, "type": "gregorian", "year": 2040 },
            { "month":  3, "date": 20, "type": "gregorian", "year": 2041 },
            { "month":  3, "date":  5, "type": "gregorian", "year": 2042 },
            { "month":  2, "date": 28, "type": "gregorian", "year": 2043 },
            { "month":  3, "date": 16, "type": "gregorian", "year": 2044 },
            { "month":  3, "date":  8, "type": "gregorian", "year": 2045 },
            { "month":  2, "date": 24, "type": "gregorian", "year": 2046 },
            { "month":  3, "date": 13, "type": "gregorian", "year": 2047 },
            { "month":  3, "date":  4, "type": "gregorian", "year": 2048 },
            { "month":  3, "date": 17, "type": "gregorian", "year": 2049 }
        ]
    }, {
        "title": "April Fools' Day",
        "href" : "http://kol.coldfront.net/thekolwiki/index.php/April_Fools%27_Day",
        "dates": [
            { "month":  3, "date":  0, "type": "gregorian" }
        ]
    }, {
        "title": "El Dia de Los Muertos Borrachos",
        "href" : "http://kol.coldfront.net/thekolwiki/index.php/El_Dia_de_Los_Muertos_Borrachos",
        "dates": [
            { "month":  4, "date":  1, "type": "kol" }
        ]
    }, {
        "title": "Generic Summer Holiday",
        "href" : "http://kol.coldfront.net/thekolwiki/index.php/Generic_Summer_Holiday",
        "dates": [
            { "month":  5, "date":  1, "type": "kol" }
        ]
    }, {
        "title": "Dependence Day",
        "href" : "http://kol.coldfront.net/thekolwiki/index.php/Dependence_Day",
        "dates": [
            { "month":  6, "date":  3, "type": "kol" },
            { "month":  6, "date":  3, "type": "gregorian" }
        ]
    }, {
        "title": "Arrrbor Day",
        "href" : "http://kol.coldfront.net/thekolwiki/index.php/Arrrbor_Day",
        "dates": [
            { "month":  7, "date":  3, "type": "kol" }
        ]
    }, {
        "title": "Labór Day",
        "href" : "http://kol.coldfront.net/thekolwiki/index.php/Lab%C3%B3r_Day",
        "dates": [
            { "month":  8, "date":  5, "type": "kol" }
        ]
    }, {
        "title": "Talk Like a Pirate Day",
        "href" : "http://kol.coldfront.net/thekolwiki/index.php/Talk_Like_a_Pirate_Day",
        "dates": [
            { "month":  8, "date": 18, "type": "gregorian" }
        ]
    }, {
        "title": "Halloween",
        "href" : "http://kol.coldfront.net/thekolwiki/index.php/Halloween",
        "dates": [
            { "month":  9, "date":  7, "type": "kol" },
            { "month":  9, "date": 30, "type": "gregorian" }
        ]
    }, {
        "title": "Feast of Boris",
        "href" : "http://kol.coldfront.net/thekolwiki/index.php/Feast_of_Boris",
        "dates": [
            { "month": 10, "date":  6, "type": "kol" },
            { "month": 10, "date": 21, "type": "gregorian", "day":  4 },
            { "month": 10, "date": 22, "type": "gregorian", "day":  4 },
            { "month": 10, "date": 23, "type": "gregorian", "day":  4 },
            { "month": 10, "date": 24, "type": "gregorian", "day":  4 },
            { "month": 10, "date": 25, "type": "gregorian", "day":  4 },
            { "month": 10, "date": 26, "type": "gregorian", "day":  4 },
            { "month": 10, "date": 27, "type": "gregorian", "day":  4 }
        ]
    }, {
        "title": "Yuletide",
        "href" : "http://kol.coldfront.net/thekolwiki/index.php/Yuletide",
        "dates": [
            { "month": 11, "date":  3, "type": "kol" }
        ]
    }, {
        "title": "Christmas",
        "href" : "http://kol.coldfront.net/thekolwiki/index.php/Crimbo",
        "dates": [
            { "month": 11, "date":  24, "type": "gregorian" }
        ]
    }, {
        "title": "Crimbo",
        "href" : "http://kol.coldfront.net/thekolwiki/index.php/Crimbo",
        "dates": [
            { "month": 11, "date":  24, "type": "gregorian" }
        ]
    }, {
        "title": "Friday the 13th",
        "href" : "http://kol.coldfront.net/thekolwiki/index.php/Friday_the_13th",
        "dates": [
            { "month":  0, "date": 12, "type": "gregorian", "day": 5 },
            { "month":  1, "date": 12, "type": "gregorian", "day": 5 },
            { "month":  2, "date": 12, "type": "gregorian", "day": 5 },
            { "month":  3, "date": 12, "type": "gregorian", "day": 5 },
            { "month":  4, "date": 12, "type": "gregorian", "day": 5 },
            { "month":  5, "date": 12, "type": "gregorian", "day": 5 },
            { "month":  6, "date": 12, "type": "gregorian", "day": 5 },
            { "month":  7, "date": 12, "type": "gregorian", "day": 5 },
            { "month":  8, "date": 12, "type": "gregorian", "day": 5 },
            { "month":  9, "date": 12, "type": "gregorian", "day": 5 },
            { "month": 10, "date": 12, "type": "gregorian", "day": 5 },
            { "month": 11, "date": 12, "type": "gregorian", "day": 5 }
        ]
    }, {
        "title": "Martin Luther King Jr. Day",
        "href" : "",
        "dates": [
            { "month":  0, "date": 14, "type": "gregorian", "day": 1 },
            { "month":  0, "date": 15, "type": "gregorian", "day": 1 },
            { "month":  0, "date": 16, "type": "gregorian", "day": 1 },
            { "month":  0, "date": 17, "type": "gregorian", "day": 1 },
            { "month":  0, "date": 18, "type": "gregorian", "day": 1 },
            { "month":  0, "date": 19, "type": "gregorian", "day": 1 },
            { "month":  0, "date": 20, "type": "gregorian", "day": 1 }
        ]
    }, {
        "title": "Presidents' Day",
        "href" : "",
        "dates": [
            { "month":  1, "date": 14, "type": "gregorian", "day": 1 },
            { "month":  1, "date": 15, "type": "gregorian", "day": 1 },
            { "month":  1, "date": 16, "type": "gregorian", "day": 1 },
            { "month":  1, "date": 17, "type": "gregorian", "day": 1 },
            { "month":  1, "date": 18, "type": "gregorian", "day": 1 },
            { "month":  1, "date": 19, "type": "gregorian", "day": 1 },
            { "month":  1, "date": 20, "type": "gregorian", "day": 1 }
        ]
    }, {
        "title": "Daylight Savings Time begins (USA)",
        "href" : "",
        "dates": [
            { "month":  2, "date":  7, "type": "gregorian", "day": 0 },
            { "month":  2, "date":  8, "type": "gregorian", "day": 0 },
            { "month":  2, "date":  9, "type": "gregorian", "day": 0 },
            { "month":  2, "date": 10, "type": "gregorian", "day": 0 },
            { "month":  2, "date": 11, "type": "gregorian", "day": 0 },
            { "month":  2, "date": 12, "type": "gregorian", "day": 0 },
            { "month":  2, "date": 13, "type": "gregorian", "day": 0 }
        ]
    }, {
        "title": "Daylight Savings Time ends (USA)",
        "href" : "",
        "dates": [
            { "month": 10, "date":  0, "type": "gregorian", "day": 0 },
            { "month": 10, "date":  1, "type": "gregorian", "day": 0 },
            { "month": 10, "date":  2, "type": "gregorian", "day": 0 },
            { "month": 10, "date":  3, "type": "gregorian", "day": 0 },
            { "month": 10, "date":  4, "type": "gregorian", "day": 0 },
            { "month": 10, "date":  5, "type": "gregorian", "day": 0 },
            { "month": 10, "date":  6, "type": "gregorian", "day": 0 },
        ]
    }, {
        "title": "Ash Wednesday",
        "href" : "",
        "dates": [
            { "month":  2, "date":  7, "type": "gregorian", "year": 2000 },
            { "month":  1, "date": 27, "type": "gregorian", "year": 2001 },
            { "month":  1, "date": 12, "type": "gregorian", "year": 2002 },
            { "month":  2, "date":  4, "type": "gregorian", "year": 2003 },
            { "month":  1, "date": 24, "type": "gregorian", "year": 2004 },
            { "month":  1, "date":  8, "type": "gregorian", "year": 2005 },
            { "month":  2, "date":  0, "type": "gregorian", "year": 2006 },
            { "month":  1, "date": 20, "type": "gregorian", "year": 2007 },
            { "month":  1, "date":  5, "type": "gregorian", "year": 2008 },
            { "month":  1, "date": 24, "type": "gregorian", "year": 2009 },
            { "month":  1, "date": 16, "type": "gregorian", "year": 2010 },
            { "month":  2, "date":  8, "type": "gregorian", "year": 2011 },
            { "month":  1, "date": 21, "type": "gregorian", "year": 2012 },
            { "month":  1, "date": 12, "type": "gregorian", "year": 2013 },
            { "month":  2, "date":  4, "type": "gregorian", "year": 2014 },
            { "month":  1, "date": 17, "type": "gregorian", "year": 2015 },
            { "month":  1, "date":  9, "type": "gregorian", "year": 2016 },
            { "month":  2, "date":  0, "type": "gregorian", "year": 2017 },
            { "month":  1, "date": 13, "type": "gregorian", "year": 2018 },
            { "month":  2, "date":  5, "type": "gregorian", "year": 2019 },
            { "month":  1, "date": 25, "type": "gregorian", "year": 2020 },
            { "month":  1, "date": 16, "type": "gregorian", "year": 2021 },
            { "month":  2, "date":  1, "type": "gregorian", "year": 2022 },
            { "month":  1, "date": 21, "type": "gregorian", "year": 2023 },
            { "month":  1, "date": 13, "type": "gregorian", "year": 2024 },
            { "month":  2, "date":  4, "type": "gregorian", "year": 2025 },
            { "month":  1, "date": 17, "type": "gregorian", "year": 2026 },
            { "month":  1, "date":  9, "type": "gregorian", "year": 2027 },
            { "month":  2, "date":  0, "type": "gregorian", "year": 2028 },
            { "month":  1, "date": 13, "type": "gregorian", "year": 2029 },
            { "month":  2, "date":  5, "type": "gregorian", "year": 2030 },
            { "month":  1, "date": 25, "type": "gregorian", "year": 2031 },
            { "month":  1, "date": 10, "type": "gregorian", "year": 2032 },
            { "month":  2, "date":  1, "type": "gregorian", "year": 2033 },
            { "month":  1, "date": 21, "type": "gregorian", "year": 2034 },
            { "month":  1, "date":  6, "type": "gregorian", "year": 2035 },
            { "month":  1, "date": 26, "type": "gregorian", "year": 2036 },
            { "month":  1, "date": 17, "type": "gregorian", "year": 2037 },
            { "month":  2, "date":  9, "type": "gregorian", "year": 2038 },
            { "month":  1, "date": 22, "type": "gregorian", "year": 2039 },
            { "month":  1, "date": 14, "type": "gregorian", "year": 2040 },
            { "month":  2, "date":  5, "type": "gregorian", "year": 2041 },
            { "month":  1, "date": 18, "type": "gregorian", "year": 2042 },
            { "month":  1, "date": 10, "type": "gregorian", "year": 2043 },
            { "month":  2, "date":  1, "type": "gregorian", "year": 2044 },
            { "month":  1, "date": 21, "type": "gregorian", "year": 2045 },
            { "month":  1, "date":  6, "type": "gregorian", "year": 2046 },
            { "month":  1, "date": 26, "type": "gregorian", "year": 2047 },
            { "month":  1, "date": 18, "type": "gregorian", "year": 2048 },
            { "month":  2, "date":  3, "type": "gregorian", "year": 2049 }
        ]
    }, {
        "title": "Palm Sunday",
        "href" : "",
        "dates": [
            { "month":  3, "date": 15, "type": "gregorian", "year": 2000 },
            { "month":  3, "date":  7, "type": "gregorian", "year": 2001 },
            { "month":  2, "date": 23, "type": "gregorian", "year": 2002 },
            { "month":  3, "date": 12, "type": "gregorian", "year": 2003 },
            { "month":  3, "date":  3, "type": "gregorian", "year": 2004 },
            { "month":  2, "date": 19, "type": "gregorian", "year": 2005 },
            { "month":  3, "date":  8, "type": "gregorian", "year": 2006 },
            { "month":  3, "date":  0, "type": "gregorian", "year": 2007 },
            { "month":  2, "date": 15, "type": "gregorian", "year": 2008 },
            { "month":  3, "date":  4, "type": "gregorian", "year": 2009 },
            { "month":  2, "date": 27, "type": "gregorian", "year": 2010 },
            { "month":  3, "date": 16, "type": "gregorian", "year": 2011 },
            { "month":  3, "date":  0, "type": "gregorian", "year": 2012 },
            { "month":  2, "date": 23, "type": "gregorian", "year": 2013 },
            { "month":  3, "date": 12, "type": "gregorian", "year": 2014 },
            { "month":  2, "date": 28, "type": "gregorian", "year": 2015 },
            { "month":  2, "date": 19, "type": "gregorian", "year": 2016 },
            { "month":  3, "date":  8, "type": "gregorian", "year": 2017 },
            { "month":  2, "date": 24, "type": "gregorian", "year": 2018 },
            { "month":  3, "date": 13, "type": "gregorian", "year": 2019 },
            { "month":  3, "date":  4, "type": "gregorian", "year": 2020 },
            { "month":  2, "date": 27, "type": "gregorian", "year": 2021 },
            { "month":  3, "date":  9, "type": "gregorian", "year": 2022 },
            { "month":  3, "date":  1, "type": "gregorian", "year": 2023 },
            { "month":  2, "date": 23, "type": "gregorian", "year": 2024 },
            { "month":  3, "date": 12, "type": "gregorian", "year": 2025 },
            { "month":  2, "date": 28, "type": "gregorian", "year": 2026 },
            { "month":  2, "date": 20, "type": "gregorian", "year": 2027 },
            { "month":  3, "date":  8, "type": "gregorian", "year": 2028 },
            { "month":  2, "date": 24, "type": "gregorian", "year": 2029 },
            { "month":  3, "date": 13, "type": "gregorian", "year": 2030 },
            { "month":  3, "date":  5, "type": "gregorian", "year": 2031 },
            { "month":  2, "date": 20, "type": "gregorian", "year": 2032 },
            { "month":  3, "date":  9, "type": "gregorian", "year": 2033 },
            { "month":  3, "date":  1, "type": "gregorian", "year": 2034 },
            { "month":  2, "date": 17, "type": "gregorian", "year": 2035 },
            { "month":  3, "date":  5, "type": "gregorian", "year": 2036 },
            { "month":  2, "date": 28, "type": "gregorian", "year": 2037 },
            { "month":  3, "date": 17, "type": "gregorian", "year": 2038 },
            { "month":  3, "date":  2, "type": "gregorian", "year": 2039 },
            { "month":  2, "date": 24, "type": "gregorian", "year": 2040 },
            { "month":  3, "date": 13, "type": "gregorian", "year": 2041 },
            { "month":  2, "date": 29, "type": "gregorian", "year": 2042 },
            { "month":  2, "date": 21, "type": "gregorian", "year": 2043 },
            { "month":  3, "date":  9, "type": "gregorian", "year": 2044 },
            { "month":  3, "date":  1, "type": "gregorian", "year": 2045 },
            { "month":  2, "date": 17, "type": "gregorian", "year": 2046 },
            { "month":  3, "date":  6, "type": "gregorian", "year": 2047 },
            { "month":  2, "date": 28, "type": "gregorian", "year": 2048 },
            { "month":  3, "date": 11, "type": "gregorian", "year": 2049 }
        ]
    }, {
        "title": "Earth Day",
        "href" : "",
        "dates": [
            { "month":  3, "date":  21, "type": "gregorian" }
        ]
    }, {
        "title": "World Donkey Day",
        "href" : "",
        "dates": [
            { "month":  4, "date":   7, "type": "gregorian" }
        ]
    }, {
        "title": "Mother's Day",
        "href" : "",
        "dates": [
            { "month":  4, "date":  7, "type": "gregorian", "day": 0 },
            { "month":  4, "date":  8, "type": "gregorian", "day": 0 },
            { "month":  4, "date":  9, "type": "gregorian", "day": 0 },
            { "month":  4, "date": 10, "type": "gregorian", "day": 0 },
            { "month":  4, "date": 11, "type": "gregorian", "day": 0 },
            { "month":  4, "date": 12, "type": "gregorian", "day": 0 },
            { "month":  4, "date": 13, "type": "gregorian", "day": 0 }
        ]
    }, {
        "title": "Memorial Day",
        "href" : "",
        "dates": [
            { "month":  4, "date": 24, "type": "gregorian", "day": 1 },
            { "month":  4, "date": 25, "type": "gregorian", "day": 1 },
            { "month":  4, "date": 26, "type": "gregorian", "day": 1 },
            { "month":  4, "date": 27, "type": "gregorian", "day": 1 },
            { "month":  4, "date": 28, "type": "gregorian", "day": 1 },
            { "month":  4, "date": 29, "type": "gregorian", "day": 1 },
            { "month":  4, "date": 30, "type": "gregorian", "day": 1 }
        ]
    }, {
        "title": "Flag Day",
        "href" : "",
        "dates": [
            { "month":  5, "date": 13, "type": "gregorian" }
        ]
    }, {
        "title": "Father's Day",
        "href" : "",
        "dates": [
            { "month":  5, "date": 14, "type": "gregorian", "day": 1 },
            { "month":  5, "date": 15, "type": "gregorian", "day": 1 },
            { "month":  5, "date": 16, "type": "gregorian", "day": 1 },
            { "month":  5, "date": 17, "type": "gregorian", "day": 1 },
            { "month":  5, "date": 18, "type": "gregorian", "day": 1 },
            { "month":  5, "date": 19, "type": "gregorian", "day": 1 },
            { "month":  5, "date": 20, "type": "gregorian", "day": 1 }
        ]
    }, {
        "title": "Mark and Amy Meyers' Wedding Anniversary",
        "href" : "https://donkeyrescue.org",
        "dates": [
            { "month":  6, "date": 17, "type": "gregorian" }
        ]
    }, {
        "title": "Labor Day (USA)",
        "href" : "",
        "dates": [
            { "month":  8, "date":  0, "type": "gregorian", "day": 1 },
            { "month":  8, "date":  1, "type": "gregorian", "day": 1 },
            { "month":  8, "date":  2, "type": "gregorian", "day": 1 },
            { "month":  8, "date":  3, "type": "gregorian", "day": 1 },
            { "month":  8, "date":  4, "type": "gregorian", "day": 1 },
            { "month":  8, "date":  5, "type": "gregorian", "day": 1 },
            { "month":  8, "date":  6, "type": "gregorian", "day": 1 }
        ]
    }, {
        "title": "Patriot Day/September 11th",
        "href" : "",
        "dates": [
            { "month":  8, "date": 10, "type": "gregorian" }
        ]
    }, {
        "title": "Yom Kippur",
        "href" : "",
        "dates": [
            { "month":  7, "date":  8, "type": "gregorian", "year": 2000 },
            { "month":  8, "date": 26, "type": "gregorian", "year": 2001 },
            { "month":  8, "date": 15, "type": "gregorian", "year": 2002 },
            { "month":  7, "date":  5, "type": "gregorian", "year": 2003 },
            { "month":  8, "date": 24, "type": "gregorian", "year": 2004 },
            { "month":  7, "date": 12, "type": "gregorian", "year": 2005 },
            { "month":  7, "date":  1, "type": "gregorian", "year": 2006 },
            { "month":  8, "date": 21, "type": "gregorian", "year": 2007 },
            { "month":  7, "date":  8, "type": "gregorian", "year": 2008 },
            { "month":  8, "date": 27, "type": "gregorian", "year": 2009 },
            { "month":  8, "date": 17, "type": "gregorian", "year": 2010 },
            { "month":  7, "date":  7, "type": "gregorian", "year": 2011 },
            { "month":  8, "date": 25, "type": "gregorian", "year": 2012 },
            { "month":  8, "date": 13, "type": "gregorian", "year": 2013 },
            { "month":  7, "date":  3, "type": "gregorian", "year": 2014 },
            { "month":  8, "date": 22, "type": "gregorian", "year": 2015 },
            { "month":  7, "date": 11, "type": "gregorian", "year": 2016 },
            { "month":  8, "date": 29, "type": "gregorian", "year": 2017 },
            { "month":  8, "date": 18, "type": "gregorian", "year": 2018 },
            { "month":  7, "date":  8, "type": "gregorian", "year": 2019 },
            { "month":  8, "date": 27, "type": "gregorian", "year": 2020 },
            { "month":  8, "date": 15, "type": "gregorian", "year": 2021 },
            { "month":  7, "date":  4, "type": "gregorian", "year": 2022 },
            { "month":  8, "date": 24, "type": "gregorian", "year": 2023 },
            { "month":  7, "date": 11, "type": "gregorian", "year": 2024 },
            { "month":  7, "date":  1, "type": "gregorian", "year": 2025 },
            { "month":  8, "date": 20, "type": "gregorian", "year": 2026 },
            { "month":  7, "date": 10, "type": "gregorian", "year": 2027 },
            { "month":  8, "date": 29, "type": "gregorian", "year": 2028 },
            { "month":  8, "date": 18, "type": "gregorian", "year": 2029 },
            { "month":  7, "date":  6, "type": "gregorian", "year": 2030 },
            { "month":  8, "date": 26, "type": "gregorian", "year": 2031 },
            { "month":  8, "date": 14, "type": "gregorian", "year": 2032 },
            { "month":  7, "date":  2, "type": "gregorian", "year": 2033 },
            { "month":  8, "date": 22, "type": "gregorian", "year": 2034 },
            { "month":  7, "date": 12, "type": "gregorian", "year": 2035 },
            { "month":  7, "date":  0, "type": "gregorian", "year": 2036 },
            { "month":  8, "date": 18, "type": "gregorian", "year": 2037 },
            { "month":  7, "date":  8, "type": "gregorian", "year": 2038 },
            { "month":  8, "date": 27, "type": "gregorian", "year": 2039 },
            { "month":  8, "date": 16, "type": "gregorian", "year": 2040 },
            { "month":  7, "date":  4, "type": "gregorian", "year": 2041 },
            { "month":  8, "date": 23, "type": "gregorian", "year": 2042 },
            { "month":  7, "date": 13, "type": "gregorian", "year": 2043 },
            { "month":  7, "date":  0, "type": "gregorian", "year": 2044 },
            { "month":  8, "date": 20, "type": "gregorian", "year": 2045 },
            { "month":  7, "date":  9, "type": "gregorian", "year": 2046 },
            { "month":  8, "date": 29, "type": "gregorian", "year": 2047 },
            { "month":  8, "date": 16, "type": "gregorian", "year": 2048 },
            { "month":  7, "date":  6, "type": "gregorian", "year": 2049 }
        ]
    }, {
        "title": "Columbus Day (USA)",
        "href" : "",
        "dates": [
            { "month":  9, "date":  7, "type": "gregorian", "day": 1 },
            { "month":  9, "date":  8, "type": "gregorian", "day": 1 },
            { "month":  9, "date":  9, "type": "gregorian", "day": 1 },
            { "month":  9, "date": 10, "type": "gregorian", "day": 1 },
            { "month":  9, "date": 11, "type": "gregorian", "day": 1 },
            { "month":  9, "date": 12, "type": "gregorian", "day": 1 },
            { "month":  9, "date": 13, "type": "gregorian", "day": 1 }
        ]
    }, {
        "title": "Columbus Day (USA)",
        "href" : "",
        "dates": [
            { "month":  9, "date":  7, "type": "gregorian", "day": 1 },
            { "month":  9, "date":  8, "type": "gregorian", "day": 1 },
            { "month":  9, "date":  9, "type": "gregorian", "day": 1 },
            { "month":  9, "date": 10, "type": "gregorian", "day": 1 },
            { "month":  9, "date": 11, "type": "gregorian", "day": 1 },
            { "month":  9, "date": 12, "type": "gregorian", "day": 1 },
            { "month":  9, "date": 13, "type": "gregorian", "day": 1 }
        ]
    }, {
        "title": "Election Day (USA)",
        "href" : "",
        "dates": [
            { "month":  10, "date":  6, "type": "gregorian", "year": 2000 },
            { "month":  10, "date":  1, "type": "gregorian", "year": 2004 },
            { "month":  10, "date":  3, "type": "gregorian", "year": 2008 },
            { "month":  10, "date":  1, "type": "gregorian", "year": 2010 },
            { "month":  10, "date":  5, "type": "gregorian", "year": 2012 },
            { "month":  10, "date":  3, "type": "gregorian", "year": 2014 },
            { "month":  10, "date":  7, "type": "gregorian", "year": 2016 },
            { "month":  10, "date":  5, "type": "gregorian", "year": 2018 },
            { "month":  10, "date":  2, "type": "gregorian", "year": 2020 },
            { "month":  10, "date":  7, "type": "gregorian", "year": 2022 },
            { "month":  10, "date":  4, "type": "gregorian", "year": 2024 },
            { "month":  10, "date":  2, "type": "gregorian", "year": 2026 },
            { "month":  10, "date":  6, "type": "gregorian", "year": 2028 },
            { "month":  10, "date":  4, "type": "gregorian", "year": 2030 },
            { "month":  10, "date":  1, "type": "gregorian", "year": 2032 },
            { "month":  10, "date":  6, "type": "gregorian", "year": 2034 },
            { "month":  10, "date":  3, "type": "gregorian", "year": 2036 },
            { "month":  10, "date":  1, "type": "gregorian", "year": 2038 },
            { "month":  10, "date":  5, "type": "gregorian", "year": 2040 },
            { "month":  10, "date":  3, "type": "gregorian", "year": 2042 },
            { "month":  10, "date":  7, "type": "gregorian", "year": 2044 },
            { "month":  10, "date":  5, "type": "gregorian", "year": 2046 },
            { "month":  10, "date":  2, "type": "gregorian", "year": 2048 }
        ]
    }, {
        "title": "Vetrans Day",
        "href" : "",
        "dates": [
            { "month":  10, "date": 10, "type": "gregorian" }
        ]
    }, {
        "title": "Vetrans Day (Observed)",
        "href" : "",
        "dates": [
            { "month":  10, "date":  9, "type": "gregorian", "day": 5 },
            { "month":  10, "date": 11, "type": "gregorian", "day": 1 }
        ]
    }, {
        "title": "New Year's Eve",
        "href" : "",
        "dates": [
            { "month":  11, "date": 30, "type": "gregorian" }
        ]
    }, {
        "title": "Christmas Eve",
        "href" : "",
        "dates": [
            { "month":  11, "date": 23, "type": "gregorian" }
        ]
    }, {
        "title": "Pearl Harbor Remembrance Day (USA)",
        "href" : "",
        "dates": [
            { "month":  11, "date": 6, "type": "gregorian" }
        ]
    }, {
        "title": "Peaceful Valley's Anniversary",
        "href" : "https://donkeyrescue.org/",
        "dates": [
            { "month":  11, "date": 1, "type": "gregorian" }
        ]
    } ];

    scope.month2Holiday = {
        "gregorian": [
            [3,20,21], [4,5,20,22,25], [6,7,20,23,25,26],
            [7,8,20,26,27], [20,28,29,30], [20,31,32], 
            [11,20,33], [20,36], [14,20,34,35,36],
            [15,20,37,38], [16,20,24,39,40,41], [18,19,20,42,43,44,45]
        ], "kol": [
            [1,2,3],[0,1,2,4],[1,2,6],
            [0,1,2,7],[1,2,9],[0,1,2,10],
            [1,2,11],[0,1,2,12],[1,2,13],
            [0,1,2,15],[1,2,16],[0,1,2,17]
        ]
    };

    /**
    scope.month2Holiday = {
        "gregorian": [],
        "kol": []
    }
    for(let i = 0; i < 12; ++i) {
        scope.month2Holiday.gregorian[i] = [];
        scope.month2Holiday.kol[i] = [];
    }

    let length = scope.holidays.length;
    scope.holidays.forEach((holiday, index) => {
        unseen = {
            "gregorian": Array(12).fill(true),
            "kol": Array(12).fill(true)
        }
        holiday.dates.forEach((date) => {
            if( unseen[date.type][date.month] ) {
                unseen[date.type][date.month] = false;
                scope.month2Holiday[date.type][date.month].push(index);
            }
        });
    });
    console.log(scope.month2Holiday);
    console.log(JSON.stringify(scope.month2Holiday));
    /**/

})(window.scope = window.scope || {});
