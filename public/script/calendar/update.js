$(function() {
    var s = this;

    const MS_PER_MINUTE = 60000;
    var d = new Date();
    d.setTime( d.getTime() - d.getTimezoneOffset() * MS_PER_MINUTE )
    s.setNow(d);

    var $calendar = $("#calendar");
    s.$calendar = {
        "container": $calendar,
        "days": $calendar.find(".calendar-box:not(.calendar-etc)"),
        "header": $calendar.find(".calendar-header"),
        "footer": $calendar.find(".calendar-footer"),
        "pic": $calendar.find(".calendar-pic"),
        "prev": $calendar.find(".calendar-prev"),
        "next": $calendar.find(".calendar-next"),
        "etc": $calendar.find(".calendar-etc")
    };

    var $display = $("#display");
    s.$display = {
        "container": $display,
        "title": $display.find(".display-title"),
        "largeImg": $display.find(".display-drawn-img"),
        "kolImg": $display.find(".display-kol-img"),
        "comment": $display.find(".display-comment"),
        "close": $display.find(".display-close"),
    };

    var $date = $("#date");
    s.$date = {
        "container": $date,
        "input": $date.find(".date-input"),
        "close": $date.find(".date-close")
    };

    s.focus = [];
}.bind(window.scope = window.scope || {}) );

$(function() {
    const MS_PER_DAY = 86400000;

    var s = this;

    /**************************************************************************/
    /**                                Window                                **/
    /**************************************************************************/
    document.onkeydown = (e) => {
        e = e || window.event;
        var keyCode = e.keyCode;
        var focus = getFocus();

        //  If right arrow key
        if( keyCode == "39" || keyCode == "40" ) {
            if( focus == "calendar" || focus == "display" ) {
                s.setNow( new Date(s.now.getTime() + 7 * MS_PER_DAY) );
                updateFocus();
            }
        }

        //  If left arrow key
        else if( keyCode == "37" || keyCode == "38" ) {
            if( focus == "calendar" || focus == "display" ) {
                s.setNow( new Date(s.now.getTime() - 7 * MS_PER_DAY) );
                updateFocus();
            }
        }

        //  If escape key
        else if( keyCode == "27" ) {
            if( focus == "display" ) {
                hideDisplay();
            }
            else if( focus == "date" ) {
                hideDate();
            }
        }

        //  If space key
        else if( keyCode == "32" || keyCode == "13" ) {
            if( focus == "calendar" ) {
                showDisplay();
            }
            else if( focus == "display" ) {
                hideDisplay();
            }
        }
    }

    /**************************************************************************/
    /**                                Focus                                 **/
    /**************************************************************************/
    var updateFocus = () => {
        var focus = getFocus();

        //  If there is no focus view then use the calendar view
        if( focus == "calendar" ) {
            updateCalendar();
        }

        //  If the focus view is display
        else if( focus == "display" ) {
            updateDisplay();
        }
    };
    var getFocus = () => {
        var length = s.focus.length;

        if( length == 0 ) {
           return "calendar"
        }

        return s.focus[length - 1]
    };
    var removeFocus = (focusName) => {
        while( s.focus.length && s.focus[s.focus.length - 1] != focusName ) {
            s.focus.pop();
        }
        if( s.focus.length ) {
            s.focus.pop();
        }
    };
    var addFocus = (focusName) => {
        s.focus.push(focusName);
    };


    /**************************************************************************/
    /**                             Date's Div                               **/
    /**************************************************************************/
    var showDate = (e) => {
        //  Add display as new focus and update focus
        addFocus("date");
        updateFocus();

        //  Actually show the display
        s.$date.container.removeClass("hide");
    }
    var hideDate = (e) => {
        //  Add display as new focus and update focus
        removeFocus("date");
        updateFocus();

        //  Actually show the display
        s.$date.container.addClass("hide");
    }

    var changeDate = function() {
        var date = new Date(this.value);
        if( !isNaN(date.getTime()) ) {
            s.setNow( new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate())) );
            hideDate();
        }
    }

    s.$date.input.on("change", changeDate);
    s.$date.input.keypress(function(e) {
        if(e.keyCode == 13) {
            changeDate.call(this);
        }
        else if(e.keyCode == 27) {
            hideDate();
        }
    });
    //  Show the date selector
    s.$calendar.header.click(() => {
        showDate();
    });
    //  Hide the date selector
    s.$date.close.click(() => {
        hideDate();
    });
    

    /**************************************************************************/
    /**                          Display's Div                               **/
    /**************************************************************************/
    var showDisplay = (e) => {
        //  Add display as new focus and update focus
        hideDate();
        addFocus("display");
        updateFocus();

        //  Actually show the display
        s.$display.container.removeClass("hide");
    };
    var updateDisplay = () => {
        var pic = s.pictures[s.daysBeg.getTime()] || s.altPicture;

        s.$display.title.html("<a href='" + pic.kolwikiURL + "'>" + pic.title + "</a>");
        s.$display.largeImg.attr("src", pic.mediumSrc);
        s.$display.kolImg.attr("src", pic.kolSrc);
        s.$display.comment.html(pic.comment);
    }
    var hideDisplay = (e) => {
        //  Remove display from focus and update focus
        removeFocus("display");
        updateFocus();

        //  Actually hide the display
        this.$display.container.addClass("hide");
    };
    s.$display.close.click(hideDisplay);

    /**************************************************************************/
    /**                          Calendar's Div                              **/
    /**************************************************************************/
    s.$calendar.pic.click(showDisplay);
    var updateCalendar = () => {
        var pic = s.pictures[s.daysBeg.getTime()] || s.altPicture;
        s.$calendar.pic.attr("src", pic.smallSrc)
            .attr("title", pic.title)
            .attr("alt", pic.smallSrc);
        s.$calendar.container.removeClass("top-left top-right bottom-left bottom-right");
        s.$calendar.container.addClass(pic.style);
        s.$calendar.footer.html("<a href='" + pic.kolwikiURL + "'>" + pic.title + "</a>");

        //  If the s.daysBeg and s.daysEnd month/year are the same, then 
        //      format date div as <Month> <Year>
        if( s.daysBeg.getUTCMonth() == s.daysEnd.getUTCMonth() ) {
            s.$calendar.header.text(this.monthNames[s.daysBeg.getUTCMonth()] + " " + s.daysBeg.getUTCFullYear());
        }
        //  If the month changes but not the year, then format 
        //      <s.daysBeg Month> - <s.daysEnd Month> <Year>
        else if( s.daysBeg.getUTCFullYear() == s.daysEnd.getUTCFullYear() ) {
            s.$calendar.header.text(
                this.monthNames[s.daysBeg.getUTCMonth()]
                    + " - " + this.monthNames[s.daysEnd.getUTCMonth()]
                    + " " + s.daysBeg.getUTCFullYear()
            );
        }
        //  If the month and year are different, the format date div 
        //      <s.daysBeg Month> <s.daysBeg Year> - <s.daysEnd Month> <s.daysEnd Year>
        else {
            s.$calendar.header.text(
                this.monthNames[s.daysBeg.getUTCMonth()]
                    + " " + s.daysBeg.getUTCFullYear()
                    + " - " + this.monthNames[s.daysEnd.getUTCMonth()]
                    + " " + s.daysEnd.getUTCFullYear()
            );
        }

        s.$calendar.days.each( (index, day) => {
            var $day = $(day);
            var date = new Date(s.daysBeg.getTime() + index * MS_PER_DAY);

            $day.find(".calendar-date").text(date.getUTCDate() + s.getPosition(date.getUTCDate()));
            $day.find(".calendar-day").text( s.dayNames[date.getUTCDay()] );

            $day.find(".calendar-kol-date").text(s.getKolDate(date));
            $day.find(".calendar-kol-year").text(s.getKolYear(date));
            $day.find(".calendar-kol-month").text(s.kolMonthNames[s.getKolMonth(date)]);

            $day.find(".calendar-holiday-list").html( s.getKolHoliday(date).map((holiday) => {
                return "<a href="+holiday.href+">"+holiday.title+"</a>";
            }).join("<br>") );
        });
    };

    s.$calendar.next.click(() => {
        s.setNow( new Date(s.now.getTime() + 7 * MS_PER_DAY) );
        updateCalendar();
    });

    s.$calendar.prev.click(() => {
        s.setNow( new Date(s.now.getTime() - 7 * MS_PER_DAY) );
        updateCalendar();
    });

    updateCalendar();
}.bind(window.scope = window.scope || {}) );
