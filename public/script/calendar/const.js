(function(scope, undefined) {

    const MS_PER_MINUTE = 60000;
    const MS_PER_DAY = 86400000;
    const DAYS_PER_WEEK = 7;
    const YEAR_START = new Date(Date.UTC(2018, 11, 23));

    scope.order = [
        "c18boss",
        "ratsworth", "bossbat", "goblinking",
        "giantskeelton", "hugeghuol", "conjoinedzmombie", "gargantulihc", "bonerdagon",
        "groar",
        "drawkward", "lordspookyraven", "protectorspectre", "ed",
        "thisdude", "theman",
        "ns", "kingralph",
        "guymadeofbees",
        "sneakypete", "bbcaptain", "corman", 
        "boris", "mooney", 
        "jarlsberg", "rainking",
        "sourceagent", "blofeld", "blue", 
        "gorgolok", "stella", "spaghetti", "lumpy", "newwave", "lopez",
        "don", 
        "motherslime", 
        "olscratch", "frosty", "oscus", "zombo", "chester", "hodgman",
        "necbromancer", "linnea", 
        "greatwolf", "fallsfromsky", "mayorghost", "zombiehoa", 
        "ukskeleton", "drunkula", 
        "advent", "unclecrimbo",
        "hermit"
    ];

    //  TODO Make stactic
    scope.pictures = {}
    scope.order.forEach((npc, index) => {
        var npc = scope.npcs[npc];
        var date = new Date( YEAR_START.getTime() + index * MS_PER_DAY * DAYS_PER_WEEK );

        scope.pictures[date.getTime()] = {
            "style": "top-right",
            "title": npc.title,
            "smallSrc": npc.calendarSmall,
            "mediumSrc": npc.calendarMedium,
            "largeSrc": npc.calendarLarge,
            "kolSrc": npc.kolFull,
            "kolwikiURL": npc.kolwikiURL,
            "time": date.getTime(),
            "comment": npc.displayComment
        };
    });

    scope.altPicture = {
        "style": "top-right",
        "title": "Image 404",
        "smallSrc": "img/small/alt-small.jpg",
        "mediumSrc": "img/medium/alt-medium.jpg",
        "largeSrc": "img/large/alt-large.jpg",
        "kolSrc": "img/kol/C10bge.gif",
        "kolwikiURL": "",
        "comment": "<p>Alternatively you can play <a href='https://www.kingdomofloathing.com'><span style='font-style: italic'>the Best Game Ever</span></a></p>"
    }

    scope.month2Holiday.gregorian.forEach((month) => {
        month.forEach((holidayIndex, monthIndex) => {
            month[monthIndex] = scope.holidays[holidayIndex];
        });
    });
    scope.month2Holiday.kol.forEach((month) => {
        month.forEach((holidayIndex, monthIndex) => {
            month[monthIndex] = scope.holidays[holidayIndex];
        });
    });

})(window.scope = window.scope || {});
