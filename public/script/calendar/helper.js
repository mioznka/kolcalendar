((s, undefined) => {
    const MS_PER_HOUR = 60000;
    const MS_PER_DAY = 86400000;
    const DAYS_PER_WEEK = 7;
    const KOL_START_DATE = new Date(Date.UTC(2003, 1, 1));

    s.getPosition = (position) => {
        switch(position) {
            case 1:
            case "1":
                return "st";
            case 2:
            case "2":
                return "nd";
            case 3:
            case "3":
                return "rd";
            default:
                return "th";
        }
    }

    s.getKolTime = function(date) {
        return Math.floor(new Date(date - this).getTime() / MS_PER_DAY);
    }.bind( KOL_START_DATE );
    s.getKolYear = (date) => {
        return Math.floor(s.getKolTime(date) / 96) + 1;
    }
    s.getKolMonth = (date) => {
        return Math.floor( (s.getKolTime(date) % 96) / 8 );
    }
    s.getKolDate = (date) => {
        return (s.getKolTime(date) % 96) % 8 + 1;
    }
    s.getKolHoliday = (date) => {
        var holidays = [];

        var tmpMonth = s.getKolMonth(date);
        var tmpDate = s.getKolDate(date) - 1;
        s.month2Holiday.kol[tmpMonth].forEach((holiday) => {
            var index = 0;
            var length = holiday.dates.length;
            var notFound = true;
            while( notFound && index < length ) {
                let tmp = holiday.dates[index];
                if( tmp.type == "kol" && tmp.month == tmpMonth && tmp.date == tmpDate ) {
                    holidays.push(holiday);
                    notFound = false;
                }
                index += 1;
            }
        });

        var tmpMonth = date.getUTCMonth();
        var tmpDate = date.getUTCDate() - 1;
        s.month2Holiday.gregorian[tmpMonth].forEach((holiday) => {
            var index = 0;
            var length = holiday.dates.length;
            var notFound = true;
            while( index < length ) {
                let tmp = holiday.dates[index];
                if( tmp.type == "gregorian" && tmp.month == tmpMonth && tmp.date == tmpDate ) {
                    if( (!("day"  in tmp) || tmp.day  == date.getUTCDay()) &&
                        (!("year" in tmp) || tmp.year == date.getUTCFullYear()) ) {
                        holidays.push(holiday);
                        notFound = false;
                    }
                }
                index += 1;
            }
        });

        return holidays;
    }

    s.now = new Date(Date.UTC(0,0));
    s.daysBeg = new Date(Date.UTC(0,0));
    s.daysEnd = new Date(Date.UTC(0,0));
    s.setNow = (date) => {
        s.now.setTime( Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate()) );
        s.daysBeg.setTime( s.now.getTime() - s.now.getUTCDay() * MS_PER_DAY );
        s.daysEnd.setTime( s.daysBeg.getTime() + MS_PER_DAY * (DAYS_PER_WEEK - 1) );
    };

})( window.scope = window.scope || {} );
