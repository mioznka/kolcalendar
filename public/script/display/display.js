(function(s, $, undefined) {
    $(() => {
        s.$thumbList = $(".thumb-list");

        var $display = $("#display");
        s.$display = {
            "container": $display,
            "title": $display.find(".display-title"),
            "link": $display.find(".display-link"),
            "drawnImg": $display.find(".display-drawn-img"),
            "kolImg": $display.find(".display-kol-img"),
            "comment": $display.find(".display-comment"),
            "close": $display.find(".display-close")
        };
    });

    $(() => {
        s.order.forEach((npc) => {
            npc = s.npcs[npc];
            if( npc ) {
                var img = $("<img></img>")
                    .appendTo(s.$thumbList)
                    .addClass("thumb")
                    .attr("src", npc.displayThumb)
                    .attr("title", npc.title)
                    .click(function(event) {
                        //  If the clicked thumb is already selected then hide 
                        //      the display and unselect the thumb
                        if( $(this).hasClass("thumb-selected") ) {
                            s.hideDisplay(this);
                        }

                        //  If the clicked thumb is not already selected then 
                        //      select the clicked thumb, show/move the display 
                        //      to clicked thumb, and update display to npc
                        else {
                            s.$thumbList.find(".thumb-selected").removeClass("thumb-selected");
                            s.updateDisplay(npc);
                            s.showDisplay(this);
                        }

                        //  Regardless toggle the selected
                        $(this).toggleClass("thumb-selected");
                    });
            }
        });

        s.$display.close.click(() => {
            s.$thumbList.find(".thumb-selected").removeClass("thumb-selected");
            s.hideDisplay();
        });
    });
})(window.scope = window.scope || {}, $);
