(function(scope, $, undefined) {

    scope.order = [
        "c18boss",
        "start",
        "ratsworth", "bossbat", "goblinking",
        "giantskeelton", "hugeghuol", "conjoinedzmombie", "gargantulihc", "bonerdagon",
        "groar",
        "drawkward", "lordspookyraven", "protectorspectre", "ed",
        "thisdude", "theman",
        "ns", "kingralph",
        "guymadeofbees",
        "sneakypete", "bbcaptain", "corman", 
        "boris", "mooney", 
        "jarlsberg", "rainking",
        "sourceagent", "blofeld", "blue", 
        "gorgolok", "stella", "spaghetti", "lumpy", "newwave", "lopez",
        "don", 
        "motherslime", 
        "olscratch", "frosty", "oscus", "zombo", "chester", "hodgman",
        "necbromancer", "linnea", 
        "greatwolf", "fallsfromsky", "mayorghost", "zombiehoa", 
        "ukskeleton", "drunkula", 
        "advent", "unclecrimbo",
        "hermit",
        "end"
    ];

})(window.scope = window.scope || {}, $);
