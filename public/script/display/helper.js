(function(s, $, undefined) {
    var resize = function() {
        var offsetTop = $(this).position().top;
        var $curr = $(this);
        while( $curr.length && $curr.position().top == offsetTop ) {
            $curr = $curr.next();
        }
        if( $curr.length ) {
            s.$display.container.insertBefore($curr);
        } else {
            s.$display.container.insertAfter( $($curr.prevObject) );
        }
    }

    s.hideDisplay = (thumbSelected) => {
        s.$display.container.addClass("hide");

        $(window).off("resize");
    }

    s.showDisplay = (thumbSelected) => {
        $(window).on("resize", resize.bind(thumbSelected));
        $(window).trigger("resize");

        s.$display.container.removeClass("hide");

        $('html, body').animate({scrollTop: $(thumbSelected).offset().top}, 300);
    }

    s.updateDisplay = (npc) => {
        s.$display.title.text(npc.title);
        s.$display.drawnImg.attr("src", npc.displayMedium);
        s.$display.drawnImg.attr("title", "Drawn " + npc.title);
        s.$display.kolImg.attr("src", npc.kolFull);
        s.$display.kolImg.attr("title", "Original " + npc.title);
        s.$display.comment.html(npc.displayComment);
        s.$display.link.attr("href", npc.kolwikiURL);
    }

})(window.scope = window.scope || {}, $);
